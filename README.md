# About this project
This project is made as an example assignment to showcase VueJS a bit. It contains a simple product list in json and you are able to add products to a list. This list can be somewhat managed. You can remove, check and move items on a list.  
View this page on gitlab pages via https://whos-the-boss.gitlab.io/boodschapp-mycha/#/dashboard

##
This project is made with [Vue Argon Dashboard](https://www.creative-tim.com/product/vue-argon-dashboard) as its base.

## Vue pros and cons
### Pros  

- Vue is a very versatile javascript framework. 
- Its learning curve is not steep
- It has a big userbase
- It is still actively maintained and developed
- State management is made easy with Vuex
- There is proper debugging tooling
- The documentation is extensive
- Native TypeScript support
- Code re-usability with components and views

### Cons
- Vue is in general smaller than react, plugin-wise
- It is nice to have flexibility and many options within a framework. When working on bigger projects, this may be(come) an issue. 
