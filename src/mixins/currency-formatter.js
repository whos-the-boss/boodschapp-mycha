export const CurrencyFormatter = {
  methods: {
    formatPrice (price, locale = "nl-NL", currency = "EUR") {
      const formatter = new Intl.NumberFormat(locale, {
        style: 'currency',
        currency: currency
      })
      return formatter.format(price);
    }
  }
}
