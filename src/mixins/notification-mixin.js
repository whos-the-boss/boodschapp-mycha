export const NotifyMixin = {
  methods: {
    notifyVue (verticalAlign, horizontalAlign, message, type, icon) {
      // type: ['', 'info', 'success', 'warning', 'danger'],
      this.$notify({
        message,
        icon: icon || 'add_alert',
        horizontalAlign: horizontalAlign,
        verticalAlign: verticalAlign,
        type
      })
    }
  }
}
