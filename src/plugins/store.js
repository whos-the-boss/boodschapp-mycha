import Vue from 'vue'
import Vuex from 'vuex'
// import axios from 'axios'
import productsjson from '@/fixtures/products.json'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        status: '',
        token: localStorage.getItem('token') || '',
        user : localStorage.getItem('user') ? JSON.parse(localStorage.getItem("user")) : '',
        products: [],
        list: [],
        checkedProducts: []
    },
    mutations: {
        auth_request(state){
            state.status = 'loading'
        },
        auth_success(state, data){
            state.status = 'success'
            state.token = data.token
            state.user = data.user
        },
        auth_error(state){
            state.status = 'error'
        },
        logout(state){
            state.status = ''
            state.token = ''
            state.user = ''
        },
        reviews(state, reviews) {
            state.reviews = reviews
        },
        recommendations(state, recommendations) {
            state.recommendations = recommendations
        },
        products(state, products) {
            state.products = products
        },
        list(state, list) {
            state.status = 'loading'
            const _list = state.list
            list['total_price'] = list.quantity * list.data.price
            _list.push(list)
            const _unique = [...new Map(_list.map(item =>
                [item['product'], item])).values()];

            // remove anything from _unique that has 0 quantity
            const filtered = _unique.filter((element) => element.quantity > 0)
            state.list = filtered
            state.status = 'success'
        },
        moveListItem(state, { data, move }) {
            let _moved = state.list
            function array_move(arr, old_index, new_index) {
                if (new_index >= arr.length) {
                    var k = new_index - arr.length + 1;
                    while (k--) {
                        arr.push(undefined);
                    }
                }
                // splice performs operations on the array in-place, so a return is not necessary
                arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
                return arr; // for testing
            }
            let currentIndex = _moved.findIndex(elem => elem.product === data.product);
            // indexes start at 0
            if (currentIndex >= 0) {
                let newIndex = currentIndex
                newIndex = (move === "up") ? newIndex -= 1 : newIndex += 1
                if (typeof newIndex !== "undefined") {
                    _moved = array_move(_moved, currentIndex, newIndex)
                }
            }
            state.list = _moved
        },
        clearState(state, obj) {
            state[obj] = []
        },
        removeFromList(state, product) {
            state.status = 'loading'
            const list = state.list
            const filtered = list.filter((element) => element.product !== product.product)
            state.list = filtered
            state.status = 'success'
        },
        checkProduct(state, list) {
            const _list = state.checkedProducts
            _list.push(list)
            const _unique = [...new Map(_list.map(item =>
                [item['product'], item])).values()];
            // remove anything from _unique that has 0 quantity
            const filtered = _unique.filter((element) => element.quantity > 0)
            state.checkedProducts = filtered
        }
    },
    actions: {
        products({commit}) {
            commit('products', productsjson);
        },
        list({commit}, list) {
            commit('list', list)
        },
        clearList({commit}) {
            commit('clearState', 'list')
        },
        clearChecklist({commit}) {
            commit('clearState', 'checkedProducts')
        },
        removeFromList({commit}, product) {
            commit('removeFromList', product)
        },
        checkProduct({commit}, product) {
            commit('checkProduct', product)
        },
        moveListItem({commit}, {data, move}) {
            commit('moveListItem', { data, move })
        }
    },
    getters : {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
        user: state => state.user,
        products: state => state.products,
        productlist: state => state.list,
        checkedProducts: state => state.checkedProducts
    }
})
